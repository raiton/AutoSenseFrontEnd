import 'package:auto_sense/services/station_manager_service.dart';
import 'package:auto_sense/utils/errors.dart';
import 'package:flutter_test/flutter_test.dart';

void main(){

  test('empty name returns error string', (){
    //setup
    Pump pump = Pump(id: 1, fuelType: "fuelType", price: 2, available: true);
    //run
    var result = Errors.getErrorForget("", "address", "city", [pump]);
    //verify results
    expect(result, "Name is mandatory");
  });


  test('empty address returns error string', (){
    //setup
    Pump pump = Pump(id: 1, fuelType: "fuelType", price: 2, available: true);
    //run
    var result = Errors.getErrorForget("name", "", "city", [pump]);
    //verify results
    expect(result, "Address is mandatory");
  });


  test('empty city returns error string', (){
    //setup
    Pump pump = Pump(id: 1, fuelType: "fuelType", price: 2, available: true);
    //run
    var result = Errors.getErrorForget("", "address", "", [pump]);
    //verify results
    expect(result, "City is mandatory");
  });


  test('correct values returns empty error string', (){
    //setup
    Pump pump = Pump(id: 1, fuelType: "fuelType", price: 2, available: true);
    //run
    var result = Errors.getErrorForget("name", "address", "city", [pump]);
    //verify results
    expect(result, "");
  });


  test('empty fuel type on any pump returns error string', (){
    //setup
    Pump pump = Pump(id: 1, fuelType: "", price: 2, available: true);
    //run
    var result = Errors.getErrorForget("", "address", "city", [pump]);
    //verify results
    expect(result, "You forgot the fuel type on one of the pumps");
  });
}