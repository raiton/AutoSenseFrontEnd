// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:auto_sense/home/home.dart';
import 'package:auto_sense/pages/station_details.dart';
import 'package:auto_sense/services/station_manager_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  testWidgets('Pressing Add Pump button adds the fields for a new pump', (WidgetTester tester) async {
    //Find widgets needed && setup dummy objects
    final addPumpButton = find.byKey(const ValueKey("addPumpButton"));
    Station station = Station(
      name: "",
      latitude: 4,
      longitude: 5,
      id: "",
      address: "",
      pumps: [],
      city: "",
    );
    //run
    await tester.pumpWidget(MaterialApp(home: StationDetails(station)));
    await tester.tap(addPumpButton);
    await tester.pump(); //rebuild widget
    //verify results
    expect(find.byKey(const ValueKey("fuelTypeTextField")), findsOneWidget);
    expect(find.byKey(const ValueKey("priceTextField")), findsOneWidget);
    expect(find.byKey(const ValueKey("dropDownAvailableUnavailablePump")), findsOneWidget);
    expect(find.byKey(const ValueKey("removePumpTrashCan")), findsOneWidget);
  });
}
