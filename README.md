# auto_sense

AutoSense Application

## Frontend requirements:
️
✔ The fuel stations are displayed on a map
✔ When clicking on a station, the station details are displayed
✔ User can update the following fields of the stations:
✔   Name 
✔   Prices
✔ User can add a new station with all of it’s details
✔ User can delete a station
✔ Requirements: usage of BLoC pattern
✔ Nice to have: unit testing
✔ Delivery: provide an APK

## Backend requirements:

✔ The backend provides RESTful APIs to serve the frontend application
✔ The backend implements API key or basic authentication (it is ok to hardcode the keys on the frontend)
✔ You can choose whether you want to store the data in a database or freshly load the data into the application memory from a file 
✔ Use the sample data provided attached for your schema definition
✔ Preferred technologies:
  ✔ Node.js with Typescript
  ✔ Express.js
✔ Deployment: We recommend using free services provided by Amazon Web Services (AWS)
