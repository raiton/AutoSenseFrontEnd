import 'package:auto_sense/main.dart';
import 'package:auto_sense/utils/app_colors.dart';
import 'package:auto_sense/utils/errors.dart';
import 'package:auto_sense/utils/my_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../services/station_manager_service.dart';
import '../utils/defaults.dart';

class StationDetails extends StatefulWidget {
  final Station? station;
  const StationDetails(this.station, {Key? key}) : super(key: key);

  @override
  _StationDetails createState() => _StationDetails();
}

class _StationDetails extends State<StationDetails> {

  final MyColors myColors = MyColors();
  final Errors errors = Errors();
  final StationManagerService _stationServices = StationManagerService();
  final DefaultValues defaultValues = DefaultValues();
  late TextEditingController nameController;
  late TextEditingController addressController;
  late TextEditingController cityController;
  late Map<String, int> selectionMap = {};
  late List<Pump> pumpList;


  @override
  void initState() {
    nameController = TextEditingController(text: widget.station?.name);
    pumpList = widget.station?.pumps ?? List.empty();
    addressController = TextEditingController(text: widget.station?.address);
    cityController = TextEditingController(text: widget.station?.city);
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    addressController.dispose();
    cityController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(10),
              child: ListView(
                children: <Widget>[
                  GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Icon(
                            Icons.close,
                            size: 40,
                            color: myColors.blue,
                          ))),
                  const SizedBox(height: 20,),
                  Text(widget.station?.id != "" ? 'Edit Station' : 'Create Station', style: defaultValues.textBold30Blue()),
                  const SizedBox(height: 20,),
                  MyTextFormField(
                    'Station Name',
                    controller: nameController,
                    // label: const Text("Station Name"),
                    maxLength: 20,
                    textInputAction: TextInputAction.next,
                    fillColor: Colors.grey.withOpacity(0.25),
                  ),
                  const SizedBox(height: 4,),
                  MyTextFormField(
                    'Station Address',
                    controller: addressController,
                    textInputAction: TextInputAction.next,
                    enabled: false,
                    fillColor: Colors.grey.withOpacity(0.25),
                  ),
                  const SizedBox(height: 4,),
                  MyTextFormField(
                    'Station City',
                    controller: cityController,
                    textInputAction: TextInputAction.next,
                    enabled: false,
                    fillColor: Colors.grey.withOpacity(0.25),
                  ),
                  const SizedBox(height: 20,),
                  Text('Pumps', style: defaultValues.textBold30Blue()),
                  const SizedBox(height: 20,),
                  Visibility(visible: !pumpList.isNotEmpty, child: Center(child: Text('--- This station currently has no pumps ---', style: defaultValues.textBold15Blue()))),
                  Visibility(
                    visible: pumpList.isNotEmpty,
                    child: SizedBox(
                      child: ListView.separated(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          separatorBuilder: (BuildContext context, int index) {
                            return const SizedBox(
                              height: 5,
                            );
                          },
                          itemCount: pumpList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Row(
                              children: [
                                Flexible(
                                  child: MyTextFormField(
                                    'Fuel Type',
                                    controller: pumpList[index].fuelType != "" ?  temporaryController(pumpList[index].fuelType) : null,
                                    key: const Key("fuelTypeTextField"),
                                    textInputAction: TextInputAction.next,
                                    onChanged: (text) => changeFuelType(text, index),
                                    fillColor: Colors.grey.withOpacity(0.25),
                                    maxLength: 11,
                                  ),
                                ),
                                const SizedBox(width: 5,),
                                Flexible(
                                  child: MyTextFormField(
                                    'Price',
                                    controller: pumpList[index].price != 0 ?  temporaryController(pumpList[index].price.toString()) : null,
                                    key: const Key("priceTextField"),
                                    onChanged: (price) => changePrice(price, index),
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(RegExp(r'[0-9.]')),
                                    ],
                                    maxLength: 5,
                                    prefixIcon: Icons.monetization_on_rounded,
                                    iconColor: myColors.blue,
                                    keyboardType: TextInputType.number,
                                    textInputAction: TextInputAction.next,
                                    fillColor: Colors.grey.withOpacity(0.25),
                                  ),
                                ),
                                const SizedBox(width: 5,),
                                DropdownButton<bool>(
                                  key: const Key("dropDownAvailableUnavailablePump"),
                                    items: const [
                                      DropdownMenuItem(child: Text("Available"), value: true,),
                                      DropdownMenuItem(child: Text("Unavailable"), value: false,),
                                    ],
                                    dropdownColor: Theme.of(context).scaffoldBackgroundColor,
                                    value: pumpList[index].available,
                                    borderRadius: const BorderRadius.all(Radius.circular(4)),
                                    onChanged: (value) => dropdownCallback(index, value),
                                ),
                                const SizedBox(width: 10,),
                                GestureDetector(
                                    child: Icon(
                                        Icons.delete,
                                        color: myColors.red,
                                        key: const Key("removePumpTrashCan"),
                                        size: 40),
                                  onTap: () => deletePump(index),
                                )
                              ],
                            );
                          }
                      ),
                    ),
                  ),
                  const SizedBox(height: 20,),
                  ElevatedButton.icon(
                    key: const Key("addPumpButton"),
                    style: ElevatedButton.styleFrom(
                      primary: myColors.blue,
                      minimumSize: const Size.fromHeight(50),
                    ),
                    icon: const Icon(Icons.add
                        , size: 32,color: Colors.white),
                    label: const Text(
                      "Add Pump",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () =>
                      setState(() {
                        pumpList.add(Pump(id: 1000 + pumpList.length + 1, fuelType: "", price: 0, available: true));
                      }),
                  ),
                  const SizedBox(height: 20,),
                  ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      primary: myColors.gold,
                      minimumSize: const Size.fromHeight(50),
                    ),
                    icon: Icon(Icons.save, size: 32,color: myColors.blue),
                    label: Text(
                      "Save station",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: myColors.blue),
                    ),
                    onPressed: () {
                      String error = Errors.getErrorForget(nameController.text, addressController.text, cityController.text, pumpList);
                      if (error != '') {
                        errors.showSnac(error, context);
                      } else {
                        saveStation().then((value) {
                          if(value){
                            Navigator.pop(context);
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>  const MyApp()));
                          }else{
                            errors.showSnac('There was a problem saving the station', context);
                          }
                        });
                      }
                    },
                  ),
                  const SizedBox(height: 20,),
                  Visibility(
                    visible: widget.station?.id != "",
                    child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red,
                        minimumSize: const Size.fromHeight(50),
                      ),
                      icon: const Icon(Icons.delete, size: 32,color: Colors.white),
                      label: const Text(
                        "Delete",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        deleteStation().then((value) {
                          if(value){
                            Navigator.pop(context);
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const MyApp()));
                          }
                        });
                        },
                    ),
                  ),
                ],
              ),
        ),
      ),
    );
  }

  Future<bool> saveStation() async{
    Station station = widget.station!;
    Station updatedStation = Station(
      name: nameController.text,
      address: addressController.text,
      city: cityController.text,
      latitude: station.latitude,
      longitude: station.longitude,
      id: station.id,
      pumps: pumpList,
    );
    if(updatedStation.id != "") {
      return await _stationServices.updateStation(updatedStation);
    }else{
      return await _stationServices.createStations(updatedStation);
    }

  }

  Future<bool> deleteStation() async{
    return await _stationServices.deleteStation(widget.station?.id);
  }

  dropdownCallback(int index, bool? selectedValue){
      setState(() => pumpList[index].available = selectedValue!);
  }

  deletePump(int index) {
    setState(() {
      pumpList.removeAt(index);
    });
  }

  changeFuelType(String text, int index) {
    setState(() {
      pumpList[index].fuelType = text.toUpperCase();
    });
  }

  changePrice(String priceText, int index) {
    num? price = num.tryParse(priceText);
    if(price == null){
      errors.showSnac("Price has to be a number", context);
    }else{
      setState(() => pumpList[index].price = price);
    }
  }

  TextEditingController temporaryController(String text) {
    TextEditingController controller = TextEditingController(text: text);
    controller.selection = TextSelection.fromPosition(TextPosition(offset: controller.text.length));
    return controller;
  }
}
