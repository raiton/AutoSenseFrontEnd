import 'package:auto_sense/home/home.dart';
import 'package:auto_sense/services/station_manager_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Google Maps Demo',
      home: MultiRepositoryProvider(
        providers: [
        RepositoryProvider(
          create: (context) => StationManagerService(),
        ),
      ],
        child: HomePage(),
      ),
    );
  }
}
