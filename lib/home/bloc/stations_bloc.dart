import 'dart:async';

import 'package:auto_sense/services/station_manager_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StationsBlock implements BlocBase{
  late final StationManagerService _stationServices;

  final StreamController<bool> _stationsController = StreamController<bool>();
  Stream get outStations => _stationsController.stream;

  final StreamController<int> _deleteController = StreamController<int>();
  Sink get inSearch => _deleteController.sink;

  StationsBlock(){
    _stationServices = StationManagerService();
    _deleteController.stream.listen((event) => _delete);
  }

  void _delete(String id) async{
    bool deleted = await _stationServices.deleteStation(id);
    _stationsController.sink.add(deleted);
  }


  @override
  void addError(Object error, [StackTrace? stackTrace]) {
    // TODO: implement addError
  }

  @override
  Future<void> close() {
    // TODO: implement close
    throw UnimplementedError();
  }

  @override
  void emit(state) {
    // TODO: implement emit
  }

  @override
  // TODO: implement isClosed
  bool get isClosed => throw UnimplementedError();

  @override
  void onChange(Change change) {
    // TODO: implement onChange
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    // TODO: implement onError
  }

  @override
  // TODO: implement state
  get state => throw UnimplementedError();

  @override
  // TODO: implement stream
  Stream get stream => throw UnimplementedError();




  // Future<int> createStations(Station station) async{
  //   String _body = json.encode(station.toJsonCreate());
  //   final response = await post(getHostName(), headers: getHeader(), body: _body);
  //   return response.statusCode;
  // }
  //
  // Future<int> updateStation(Station station) async{
  //   String id = station.id;
  //   String _body = json.encode(station.toJson());
  //   final response = await put(getHostName(target: id), headers: getHeader(), body: _body);
  //   return response.statusCode;
  // }
  //
  // Future<bool> deleteStation(String? id) async{
  //   Map params = {"id": id};
  //   String _body = json.encode(params);
  //   final response = await delete(getHostName(target: id!),headers: getHeader(), body: _body);
  //   if(response.statusCode == 200){
  //     return true;
  //   }
  //   return false;
  // }
}