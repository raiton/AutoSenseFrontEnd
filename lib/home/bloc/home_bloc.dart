import 'package:auto_sense/services/station_manager_service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'home_state.dart';
part 'home_event.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState>{

  final StationManagerService _stationService;

  HomeBloc(this._stationService) : super(HomeLoadingState()){
    on<LoadApiEvent>((event, emit) async{
      final stations = await _stationService.getStations();
      emit(HomeLoadedState(stations));
    });
  }
}