part of 'home_bloc.dart';

abstract class HomeState extends Equatable{
  const HomeState();
}

class HomeLoadingState extends HomeState{
  @override
  List<Object> get props => [];
}

class HomeLoadedState extends HomeState{

  final List<Station> stations;

  const HomeLoadedState(this.stations);

  @override
  List<Object?> get props => [stations];
}