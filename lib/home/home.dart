import 'dart:async';

import 'package:auto_sense/home/bloc/home_bloc.dart';
import 'package:auto_sense/services/location_service.dart';
import 'package:auto_sense/services/station_manager_service.dart';
import 'package:auto_sense/utils/app_colors.dart';
import 'package:auto_sense/utils/defaults.dart';
import 'package:clippy_flutter/triangle.dart';
import 'package:custom_info_window/custom_info_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:auto_sense/utils/errors.dart';

import '../pages/station_details.dart';

class HomePage extends StatefulWidget {

  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final LocationService _locationService = LocationService();
  final Completer<GoogleMapController> _controller = Completer();
  final Set<Marker> _markers = <Marker>{};
  final MyColors myColors = MyColors();
  Errors errors = Errors();
  DefaultValues defaultValues = DefaultValues();
  bool windowOpen = false;
  final CameraPosition _defaultCameraLocation = const CameraPosition(target: LatLng(47.3809116, 8.4790686) ,zoom: 13.5);

  final CustomInfoWindowController _customInfoWindowController = CustomInfoWindowController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(RepositoryProvider.of<StationManagerService>(context))..add(LoadApiEvent()),
      child: Scaffold(
        body: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state){
          if(state is HomeLoadingState){
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if(state is HomeLoadedState){
            populateMarkers(state.stations);
            return Column(
              children: [
                /*Row(
                  children: [
                    Expanded(child: Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: TextFormField(
                        controller: _searchController,
                        textCapitalization: TextCapitalization.words,
                        decoration: InputDecoration(hintText: 'Search by City'),
                        onChanged: (value) {
                          print(value);
                        },
                      ),
                    )),
                    IconButton(onPressed: (){
                      LocationService().getPlaceId(_searchController.text);
                    },
                      icon: const Icon(Icons.search),),
                  ],
                ),*/
                Expanded(
                  child: Stack(
                    children: [
                      GoogleMap(
                        onTap: (point) => {
                          if(windowOpen){
                            _customInfoWindowController.hideInfoWindow!(),
                            windowOpen = false,
                          }else{
                            _setMarker(point, state.stations),
                          }
                        },
                        onCameraMove: (position) => _customInfoWindowController.onCameraMove!(),
                        mapType: MapType.normal,
                        markers: _markers,
                        initialCameraPosition: _defaultCameraLocation,
                        onMapCreated: (GoogleMapController controller) {
                          _controller.complete(controller);
                          _customInfoWindowController.googleMapController = controller;
                        },
                      ),
                      CustomInfoWindow(
                        controller: _customInfoWindowController,
                        height: 300,
                        width: 700,
                        // offset: 5,
                      ),
                    ],
                  ),
                ),
              ],
            );
          }
            return Container();
        },),
      ),
    );
  }

  void _setMarker(LatLng point, List<Station> stations) async{
    Placemark placeMark = await _locationService.getPlaceMarkFromCoordinates(LatLng(point.latitude, point.longitude));
    Station station = Station(
      name: "",
      latitude: point.latitude,
      longitude: point.longitude,
      id: "",
      address: placeMark.street!,
      pumps: [],
      city: placeMark.locality!,
    );
    Navigator.push(context, MaterialPageRoute(builder: (context) => StationDetails(station)));
  }

  void populateMarkers(List<Station> stations) {
    for(Station station in stations){
      _markers.add(
        Marker(
          markerId: MarkerId(station.id),
          position: LatLng(station.latitude, station.longitude),
          onTap: (){
            windowOpen = true;
            _customInfoWindowController.addInfoWindow!(
              Column(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: myColors.blue,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(station.name.toUpperCase(),
                              style: defaultValues.textBold16Gold(),
                            ),
                            const SizedBox(height: 10,),
                            Text(station.address + ', ' + station.city,
                              style: defaultValues.text15(),
                            ),
                            Visibility(
                              visible: station.pumps.isNotEmpty,
                              child: Column(
                                children: [
                                  ListView.builder(
                                    physics: const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: station.pumps.length < 4 ? station.pumps.length : 3,
                                      itemBuilder: (BuildContext context, int index) {
                                        return Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(station.pumps[index].fuelType,
                                                  style: defaultValues.textBold15(),
                                                ),
                                            Text('\$' + station.pumps[index].price.toStringAsFixed(2),
                                                style: defaultValues.textBold15(),
                                              ),
                                            Icon((station.pumps[index].available ? Icons.check : Icons.close), color: Colors.white,)
                                          ],
                                        );
                                      }
                                  ),
                                  Visibility(
                                    visible: station.pumps.length > 3,
                                    child: Text("...",
                                      style: defaultValues.textBold15(),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 15),
                            ElevatedButton(
                              key: const Key("stationInfoButton"),
                              style: ElevatedButton.styleFrom(primary: myColors.gold),
                              child: Text("Station info", style: TextStyle(color: myColors.blue, fontSize: 16),),
                              onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => StationDetails(station))),
                            ),
                          ],
                        ),
                      ),
                      width: 200,
                      height: double.infinity,
                    ),
                  ),
                  Triangle.isosceles(
                    edge: Edge.BOTTOM,
                    child: Container(
                      color: myColors.blue,
                      width: 20.0,
                      height: 15.0,
                    ),
                  ),
                ],
              ),
              LatLng(station.latitude, station.longitude),
            );
          },
          icon: BitmapDescriptor.defaultMarkerWithHue(220),
        ),
      );
     }
  }
}
