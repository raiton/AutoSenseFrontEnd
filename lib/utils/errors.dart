import 'package:auto_sense/services/station_manager_service.dart';
import 'package:flutter/material.dart';

import 'app_colors.dart';

class Errors {
  final MyColors myColors = MyColors();

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> showSnac(split, context) {
    return ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(split),
          backgroundColor: myColors.gold,
          duration: const Duration(seconds: 2),
        )
    );
  }

  static String getErrorForget(String name, String address, String city, List<Pump> pumpList){
    String error = '';
    if(name == ""){
      error = 'Name is mandatory';
    }
    if(address == ""){
      error = 'Address is mandatory';
    }
    if(city == ""){
      error = 'City is mandatory';
    }
    for (var element in pumpList) {
      if(element.fuelType == ""){
        error = 'You forgot the fuel type on one of the pumps';
      }
    }
    return error;
  }
}
