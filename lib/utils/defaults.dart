import 'package:auto_sense/utils/app_colors.dart';
import 'package:flutter/material.dart';

class DefaultValues {

  MyColors myColors = MyColors();

  TextStyle textBold15(){
    return const TextStyle(
        color: Colors.white,
        fontSize: 15,
        fontWeight: FontWeight.bold
    );
  }

  TextStyle textBold15Blue(){
    return TextStyle(
        color: myColors.blue,
        fontSize: 15,
        fontWeight: FontWeight.bold
    );
  }

  TextStyle textBold16Gold(){
    return TextStyle(
        color: myColors.gold,
        fontSize: 20,
        fontWeight: FontWeight.bold
    );
  }

  TextStyle textBold30Blue(){
    return TextStyle(
        color: myColors.blue,
        fontSize: 30,
        fontWeight: FontWeight.bold
    );
  }

  TextStyle text15(){
    return const TextStyle(
        color: Colors.white,
        fontSize: 15,
    );
  }
}