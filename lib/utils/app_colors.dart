import 'package:flutter/material.dart';

class MyColors{
  Color blue = const Color(0xff114b5f);
  Color green = const Color(0xff1a936f);
  Color softGreen = const Color(0xff88d498);
  Color oliveGreen = const Color(0xffc6dabf);
  Color gold = const Color(0xffD4af3f);
  Color red = Colors.red;
}