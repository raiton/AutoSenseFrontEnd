import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_colors.dart';

class MyTextFormField extends StatefulWidget {
  final TextInputAction textInputAction;
  final String hint;
  final Widget? label;
  final TextInputType keyboardType;
  final TextEditingController? controller;
  final void Function(String)? onChanged;
  final void Function()? onTap;
  final void Function()? onIconPress;
  final String? Function(String?)? validator;
  final IconData? sufixIcon;
  final IconData? prefixIcon;
  final Color? iconColor;
  final int minLines;
  final int maxLines;
  final bool readOnly;
  final bool enabled;
  final String initialValue;
  final Color textColor;
  final Color? fillColor;
  final FontWeight fontWeight;
  final bool obscureText;
  final int? maxLength;
  final List<TextInputFormatter>? inputFormatters;

  const MyTextFormField(this.hint, {Key? key,this.controller, this.iconColor, this.obscureText = false, this.textInputAction = TextInputAction.next, this.fillColor, this.fontWeight = FontWeight.normal, this.textColor = Colors.black, this.keyboardType = TextInputType.text, this.onChanged, this.onTap, this.sufixIcon, this.prefixIcon, this.onIconPress, this.readOnly = false, this.initialValue = '', this.enabled = true, this.minLines = 1, this.maxLines = 1, this.maxLength, this.validator, this.inputFormatters, this.label}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyTextFormFieldState();
}

class _MyTextFormFieldState extends State<MyTextFormField>
{
  MyColors myColors = MyColors();

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLength: widget.maxLength,
      obscureText: widget.obscureText,
      textInputAction: widget.textInputAction,
      style: TextStyle(color: widget.textColor, fontWeight: widget.fontWeight),
      onChanged: widget.onChanged,
      minLines: widget.minLines,
      maxLines: widget.maxLines,
      keyboardType: widget.keyboardType,
      inputFormatters: widget.inputFormatters,
      controller: widget.controller,
      cursorColor: myColors.blue,
      readOnly: widget.readOnly,
      enabled: widget.enabled,
      validator: widget.validator,
      decoration: InputDecoration(
        counterText: "",
        contentPadding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 8.0),
        label: widget.label,
        hintText: widget.hint,
        hintStyle: TextStyle(color: myColors.blue,),
        labelStyle: TextStyle(color: myColors.blue,),
        fillColor: widget.fillColor ?? Theme.of(context).scaffoldBackgroundColor,
        filled: true,
        suffixIcon: widget.sufixIcon != null ? IconButton(icon: Icon(widget.sufixIcon, color:myColors.gold), onPressed: widget.onIconPress,) : null,
        prefixIcon: widget.prefixIcon != null ? Icon(widget.prefixIcon,color: widget.iconColor) : null,
        enabledBorder: const UnderlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            borderSide: BorderSide(color: Colors.transparent)
        ),
        focusedBorder: UnderlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(4)),
          borderSide: BorderSide(width: 8, color: myColors.blue,),
        ),),
      onTap: widget.onTap,
    );
  }
}


