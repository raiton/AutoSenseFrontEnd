
import 'package:auto_sense/services/station_manager_key.dart';
import 'package:http/http.dart';
import 'dart:convert';

List<Station> stationsFromJson(String str) => List<Station>.from(json.decode(str).map((x) => Station.fromJson(x)));

String stationsToJson(List<Station> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Station {
  Station({
    required this.id,
    required this.name,
    required this.address,
    required this.city,
    required this.latitude,
    required this.longitude,
    required this.pumps,
  });

  String id;
  String name;
  String address;
  String city;
  double latitude;
  double longitude;
  List<Pump> pumps;

  factory Station.fromJson(Map<String, dynamic> json) => Station(
    id: json["id"],
    name: json["name"],
    address: json["address"],
    city: json["city"],
    latitude: json["latitude"].toDouble(),
    longitude: json["longitude"].toDouble(),
    pumps: List<Pump>.from(json["pumps"].map((x) => Pump.fromJson(x))),
    );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "address": address,
    "city": city,
    "latitude": latitude,
    "longitude": longitude,
    "pumps": List<dynamic>.from(pumps.map((x) => x.toJson())),
  };

  Map<String, dynamic> toJsonCreate() => {
    "name": name,
    "address": address,
    "city": city,
    "latitude": latitude,
    "longitude": longitude,
    "pumps": List<dynamic>.from(pumps.map((x) => x.toJson())),
  };
}

class Pump {
  Pump({
    required this.id,
    required this.fuelType,
    required this.price,
    required this.available,
  });

  int id;
  String fuelType;
  num price;
  bool available;

  factory Pump.fromJson(Map<String, dynamic> json) => Pump(
    id: json["id"],
    fuelType: json["fuel_type"],
    price: json["price"].toDouble(),
    available: json["available"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fuel_type": fuelType,
    "price": price,
    "available": available,
  };
}


class StationManagerService{

  final String endpoint = "http://3.90.66.246:1337/api/stations/"; // Production
  // final String endpoint = "http://10.0.2.2:1337/api/stations/"; // Local

  Future<List<Station>> getStations() async{
    final response = await get(getHostName(), headers: getHeader());
    if(response.statusCode == 200){
      final stations = stationsFromJson(response.body);
      return stations;
    }else{
      print("Error " + response.statusCode.toString() + " | " + response.body);
      return List.empty();
    }
  }

  Future<bool> createStations(Station station) async{
    String _body = json.encode(station.toJsonCreate());
    final response = await post(getHostName(), headers: getHeader(), body: _body);
    return wasSuccessful(response);
  }

  Future<bool> updateStation(Station station) async{
    String id = station.id;
     String _body = json.encode(station.toJson());
    final response = await put(getHostName(target: id), headers: getHeader(), body: _body);
    return wasSuccessful(response);
  }

  Future<bool> deleteStation(String? id) async{
    Map params = {"id": id};
    String _body = json.encode(params);
    final response = await delete(getHostName(target: id!),headers: getHeader(), body: _body);
    return wasSuccessful(response);
  }

  getHeader() {
    return {
      "Content-Type": "application/json",
      "Authorization": StationApiKey.getKey(),
    };
  }

  getHostName({String target = ""}){
    return Uri.parse(endpoint + target);
  }

  bool wasSuccessful(response) {
    if(response.statusCode == 200){
      return true;
    }
    return false;
  }
}
