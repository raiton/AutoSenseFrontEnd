import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';



class LocationService{

  Future<Placemark> getPlaceMarkFromCoordinates(LatLng latLng) async{
    List<Placemark> placemarks = await placemarkFromCoordinates(latLng.latitude, latLng.longitude);
    return placemarks.first;
  }
}
