class StationApiKey{
  static const String apiKey = 'autoSenseAuthenticationKey';
  static String getKey() => 'Bearer ' + apiKey;
}